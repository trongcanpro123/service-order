
import mongoose from 'mongoose';
import humps from 'humps';
import _ from 'lodash';
import pluralize from 'pluralize';

import commonEvent from '../constants/eventConstant';
import config from 'config';

const version = process.env.VERSION || config.get('version');

export const nomalizeModelName = (modelName) => { // "v1/serviceOrders" OR  "serviceOrders" => "v1.serviceOrder"
  const versionPrefix = `v${version}`;
  let singularModelName = pluralize.singular(modelName).replace('/', '.');

  if (singularModelName.indexOf(`${versionPrefix}.`) > -1) {
    return singularModelName;
  }

  return `${versionPrefix}.${singularModelName}`;  
}

const toUpperCaseStateName = (state) => {
  return humps.decamelize(state).toUpperCase();
}

export const getEvent = (modelName, state) => {
  return `${nomalizeModelName(modelName)}.${state}`;
  
};

export const getEventList = (model, stateList) => {
  const modelName = nomalizeModelName(model);
  const eventList = [];

  if (_.isArray(stateList)) {
    eventList.forEach((state) => {
      eventList.push({
        [toUpperCaseStateName(state)]: `${modelName}.${state}`,
      });
    });
  }

  return eventList;
};

export const createDefaultEventList = (model) => {
  const modelName = nomalizeModelName(model);
  const eventList = {};

  eventList.CREATED = `${modelName}.${commonEvent.CREATED}`
  eventList.UPDATED = `${modelName}.${commonEvent.UPDATED}`
  eventList.DELETED = `${modelName}.${commonEvent.DELETED}`
  eventList.DELETE_REJECTED = `${modelName}.${commonEvent.DELETE_REJECTED}`
  eventList.HANDLED = `${modelName}.${commonEvent.HANDLED}`
  eventList.CAN_NOT_HANDLED = `${modelName}.${commonEvent.CAN_NOT_HANDLED}`

  return eventList;
};

export const createEvent = (origin, type, payload, correlationId) => ({
  origin,
  type,
  payload,
  correlationId,
});

export const swaggerEvent = () => {
  // list swagger event => response service api / model
}

export const handleCommonEvent = (DataModel, model, event, taskList) => {
  const deleteRejectedEvent = getEvent(model, commonEvent.DELETE_REJECTED);
  const userUpdatedEvent = getEvent('users', commonEvent.UPDATED);
  const { type, payload } = event;

  switch (type) {
    case userUpdatedEvent: {
      const user = payload.newData;

      taskList.push((cb) => {
        DataModel.updateMany(
          { createdBy: user._id },
          {
            createdByUserName: user.userName,
            createdByFullName: user.fullName,
          },
          cb,
        );
      });

      taskList.push((cb) => {
        DataModel.updateMany(
          { updatedBy: user._id },
          {
            updatedByUserName: user.userName,
            updatedByFullName: user.fullName,
          },
          cb,
        );
      });

      break;
    }

    case deleteRejectedEvent: {
      // TODO: notify user
  
      taskList.push((cb) => {
        DataModel.updateOne({
          _id: payload,
          deleted: true,
        },
        {
          $unset: {
            deleteRejectTimeOut: "",
            deleted: "",
            deletedAt: "",
            deletedBy: "",
            deletedByUserName: "",
            deletedByFullName: "",
          }
        },
        cb,
        );
      });
    }

    default: {
      break;
    }
  } // switch (event.type)
}