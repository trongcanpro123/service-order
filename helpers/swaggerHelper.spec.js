import { generateSwagger } from './swaggerHelper';
import '../services/models/departmentModel';

describe('Test swaggerHelper', () => {

  test('GenerateSwagger is defined', () => {
    expect(generateSwagger).toBeDefined();
  });

  const result = generateSwagger('departments', '1', 'swagger description');

  test('GenerateSwagger returns swagger', () => {
    expect(result).toBeDefined();
  });

  test('Returned swagger is object', () => {
    expect(typeof result).toBe('object');
  });

  test('Returned swagger has main structure', () => {
    expect(result).toHaveProperty('swagger');
    expect(result).toHaveProperty('host');
    expect(result).toHaveProperty('basePath');
    expect(result).toHaveProperty('info');
    expect(result).toHaveProperty('schemes');
    expect(result).toHaveProperty('paths');
    expect(result).toHaveProperty('definitions');
  });

});
