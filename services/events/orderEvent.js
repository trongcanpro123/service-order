import { createDefaultEventList, getEvent } from '../../helpers/eventHelper';
import { commonEvent } from '../../constants/eventConstant';

const model = 'v2/sOrder';
const eventList = createDefaultEventList(model);

eventList.DONE = getEvent(model, commonEvent.DONE);
eventList.CREATED_BY_API = getEvent(model, commonEvent.CREATED_BY_API);
eventList.CREATED_BY_API_V2 = getEvent(model, commonEvent.CREATED_BY_API_V2);
eventList.PROCESS = getEvent(model, commonEvent.PROCESS);

export default eventList;
