import _ from 'lodash';

import orderEvent from './orderEvent';

const eventBundle = [];
const eventList = [];

eventBundle.push(orderEvent);

eventBundle.forEach(event => {
  _.values(event).forEach((eventKey) => {
    eventList.push(eventKey);
  });
});

export default eventList;
