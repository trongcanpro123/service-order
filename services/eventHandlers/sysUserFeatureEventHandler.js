import mongoose from 'mongoose';
import _ from 'lodash';
import async from 'async';
import config from 'config';
import userEvent from '../events/userEvent';
import { getUserFeature, getUserAccessList } from '../../helpers/policyHelper';
import { USER_FEATURE } from '../../constants/policyConstant';
import companyEvent from '../events/companyEvent';
import departmentEvent from '../events/departmentEvent';
import divisionEvent from '../events/divisionEvent';
import { SMC } from '../constants/marketingCampaignConstant';
import marketingEvent from '../events/marketingEvent';
import { BD, VAR, FAF, BOD, VMDF, VJFD, COOP, SFDC, SMC_MANAGER } from '../constants/marketingConstant';
import date from 'date-and-time';
import { sendEmail } from '../../helpers/deploymentHelper';
import priceEvent from '../events/priceEvent';
import { PM, SENT, PENDDING_BD, PENDDING_BOD, APPROVED, CANCLED } from '../constants/constant';

export const model = 'sysUserFeatures';

export const servedEvent = [
  userEvent.CREATED,// user updated => create user access list by policy
  userEvent.UPDATED, // user updated => update user access list by policy
  userEvent.DELETED, // user deleted => delete user user feature / access list.

  // companyEvent.CREATED, // [..] staffList empty => skip
  companyEvent.UPDATED, // TODO: companyEvent.DELETED

  departmentEvent.CREATED,
  departmentEvent.UPDATED,

  divisionEvent.CREATED,
  divisionEvent.UPDATED,

  priceEvent.SEND_MAIL,

  marketingEvent.SENTSMC,
  marketingEvent.APPROVED,
  marketingEvent.SMCSENT,
];
const domain = process.env.DOMAIN || config.get('domain');

const UserFeatureModel = mongoose.model(model);
const PolicyModel = mongoose.model('sysPolicies');
const AccessList = mongoose.model('sysAccessList');

const getUserRelatedAccessList = async (userFeature) => {
  let accessList = [];
  const feature = (userFeature && userFeature.userFeatureList) ? userFeature.userFeatureList.find((feature) => feature.featureName === USER_FEATURE.FUNCTION_LIST) : [];

  if (feature) {
    const functionList = feature.value;

    if (functionList.length > 0) {
      const policyList = await PolicyModel.find({
        functionId: {
          $in: functionList, // [..] get related policy
        }
      });

      if (_.isArray(policyList)) {
        policyList.forEach(p => {
          accessList = _.concat(accessList, getUserAccessList(userFeature, p));
        });
      }
    }
  }

  return accessList;
}

export const handle = async (event, eventHandleCallBack) => {
  let accessList = [];

  try {
    switch (event.type) {
      case userEvent.CREATED: {
        const user = event.payload.newData;
        const dataObject = new UserFeatureModel();
        const userFeature = getUserFeature(user);

        Object.entries(userFeature).forEach(([key, value]) => {
          dataObject[key] = value;
        });

        await dataObject.save();

        if (user.active) {
          accessList = await getUserRelatedAccessList(userFeature);
        }

        break;
      }

      case userEvent.UPDATED: {
        const user2 = event.payload.newData;
        const userFeature2 = getUserFeature(user2);
        const userId = user2._id;

        await UserFeatureModel.deleteMany({ userId });
        await AccessList.deleteMany({ userId });

        const dataObject = new UserFeatureModel();

        Object.entries(userFeature2).forEach(([key, value]) => {
          dataObject[key] = value;
        });

        await dataObject.save();

        accessList = await getUserRelatedAccessList(userFeature2);

        break;
      }

      case userEvent.DELETED: {
        const userId = event.payload.newData._id;

        await UserFeatureModel.deleteMany({ userId });
        await AccessList.deleteMany({ userId });

        break;
      }

      case companyEvent.UPDATED: {
        const { oldData, newData } = event.payload;
        const oldCeoUserId = oldData.ceoUserId;
        const newCeoUserId = newData.ceoUserId;
        const companyId = newData._id;

        const oldStaffList = oldData.staffList;
        const newStaffList = newData.staffList;
        const subTaskList = [];
        const changedUserList = [];
        const oldDepartmentList = [];
        const oldDepartmentTypeList = [];
        const oldDivisionList = [];
        const oldDivisionTypeList = [];

        oldStaffList.forEach((staff) => {
          changedUserList.push(staff.userId);

          oldDepartmentList.push(staff.departmentId);
          oldDepartmentTypeList.push(staff.departmentTypeId);

          oldDivisionList.push(staff.divisionId);
          oldDivisionTypeList.push(staff.divisionTypeId);
        });

        subTaskList.push((cb) => { // remove companyId from ALL user
          UserFeatureModel.updateMany(
            {
              userFeatureList: {
                $elemMatch: {
                  featureName: USER_FEATURE.COMPANY_LIST,
                },
              },

              userId: {
                $in: changedUserList,
              },
            },
            {
              $pull: {
                'userFeatureList.$.value': companyId,
              }
            },
            cb
          );
        });

        _.uniq(oldDepartmentList).forEach((departmentId) => {
          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                // userId, // [..] all user
                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DEPARTMENT_LIST,
                  },
                },

                userId: {
                  $in: changedUserList,
                },
              },

              {
                $pull: {
                  'userFeatureList.$.value': departmentId,
                }
              },
              cb
            );
          });
        });

        _.uniq(oldDepartmentTypeList).forEach((departmentTypeId) => {
          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                // userId, // [..] all user
                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DEPARTMENT_TYPE_LIST,
                  },
                },

                userId: {
                  $in: changedUserList,
                },
              },

              {
                $pull: {
                  'userFeatureList.$.value': departmentTypeId,
                }
              },
              cb
            );
          });
        });

        _.uniq(oldDivisionList).forEach((divisionId) => {
          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                // userId, // [..] all user
                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DIVISION_LIST,
                  },
                },

                userId: {
                  $in: changedUserList,
                },
              },

              {
                $pull: {
                  'userFeatureList.$.value': divisionId,
                }
              },
              cb
            );
          });
        });

        _.uniq(oldDivisionTypeList).forEach((divisionTypeId) => {
          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                // userId, // [..] all user
                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DIVISION_TYPE_LIST,
                  },
                },

                userId: {
                  $in: changedUserList,
                },
              },

              {
                $pull: {
                  'userFeatureList.$.value': divisionTypeId,
                }
              },
              cb
            );
          });
        });

        if (oldCeoUserId) {
          changedUserList.push(oldCeoUserId);

          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId: oldCeoUserId,

                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.COMPANY_LIST,
                  },
                }
              },
              {
                $pull: {
                  'userFeatureList.$.value': companyId,
                }
              },
              cb,
            );
          });
        }

        if (newCeoUserId) {
          changedUserList.push(newCeoUserId);

          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId: newCeoUserId,
                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.COMPANY_LIST,
                  },
                }
              },
              {
                $addToSet: {
                  'userFeatureList.$.value': companyId,
                }
              },
              cb
            );
          });
        }

        newStaffList.forEach((staff) => {
          const {
            userId,
            departmentId, departmentTypeId,
            divisionId, divisionTypeId,
          } = staff;

          changedUserList.push(userId);

          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId,
                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.COMPANY_LIST,
                  },
                }
              },
              {
                $addToSet: {
                  'userFeatureList.$.value': companyId,
                }
              },
              cb
            );
          });

          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId,
                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DEPARTMENT_LIST,
                  },
                },
              },

              {
                $addToSet: {
                  'userFeatureList.$.value': departmentId,
                }
              },
              cb
            );
          });

          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId,
                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DEPARTMENT_TYPE_LIST,
                  },
                },
              },

              {
                $addToSet: {
                  'userFeatureList.$.value': departmentTypeId,
                }
              },
              cb
            );
          });

          if (divisionId) {
            subTaskList.push((cb) => {
              UserFeatureModel.updateMany(
                {
                  userId,
                  userFeatureList: {
                    $elemMatch: {
                      featureName: USER_FEATURE.DIVISION_LIST,
                    },
                  },
                },
                {
                  $addToSet: {
                    'userFeatureList.$.value': divisionId,
                  }
                },
                cb
              );
            });

            subTaskList.push((cb) => {
              UserFeatureModel.updateMany(
                {
                  userId,
                  userFeatureList: {
                    $elemMatch: {
                      featureName: USER_FEATURE.DIVISION_TYPE_LIST,
                    },
                  },
                },
                {
                  $addToSet: {
                    'userFeatureList.$.value': divisionTypeId,
                  }
                },
                cb
              );
            });
          } // if (divisionId)
        });

        subTaskList.push((cb) => { // in the end, re-calc user related access list
          UserFeatureModel.find(
            {
              userId: {
                $in: _.uniq(changedUserList),
              }
            },

            async (error, userFeatureList) => {
              if (error) {
                cb(error);
              } else {
                if (_.isArray(userFeatureList)) {
                  userFeatureList.forEach(async (userFeature) => {
                    accessList = _.concat(accessList, await getUserRelatedAccessList(userFeature));
                  });

                  cb(null);
                }
              }
            }
          );
        });

        if (subTaskList.length > 0) {
          async.series(subTaskList, eventHandleCallBack);
        } else {
          eventHandleCallBack(null);
        }

        return;
      }

      case departmentEvent.CREATED: {
        const { newData } = event.payload;
        const departmentId = newData._id;
        const { companyId, departmentTypeId } = newData;
        const { staffList } = newData;

        const subTaskList = [];

        staffList.forEach((staff) => {
          const { userId, divisionId, divisionTypeId } = staff;

          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId,

                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.COMPANY_LIST,
                  },
                }
              },

              {
                $addToSet: {
                  'userFeatureList.$.value': companyId,
                }
              },
              cb
            );
          });

          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId,

                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DEPARTMENT_LIST,
                  },
                },
              },

              {
                $addToSet: {
                  'userFeatureList.$.value': departmentId,
                }
              },
              cb
            );
          });

          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId,

                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DEPARTMENT_TYPE_LIST,
                  },
                },
              },

              {
                $addToSet: {
                  'userFeatureList.$.value': departmentTypeId,
                }
              },
              cb
            );
          });

          if (divisionId) {
            subTaskList.push((cb) => {
              UserFeatureModel.updateMany(
                {
                  userId,

                  userFeatureList: {
                    $elemMatch: {
                      featureName: USER_FEATURE.DIVISION_LIST,
                    },
                  },
                },

                {
                  $addToSet: {
                    'userFeatureList.$.value': divisionId,
                  }
                },
                cb
              );
            });

            subTaskList.push((cb) => {
              UserFeatureModel.updateMany(
                {
                  userId,

                  userFeatureList: {
                    $elemMatch: {
                      featureName: USER_FEATURE.DIVISION_TYPE_LIST,
                    },
                  },
                },

                {
                  $addToSet: {
                    'userFeatureList.$.value': divisionTypeId,
                  }
                },
                cb
              );
            });
          } // if (divisionId)
        });

        if (subTaskList.length > 0) {
          async.series(subTaskList, eventHandleCallBack);
        } else {
          eventHandleCallBack(null);
        }

        return;
      }

      case departmentEvent.UPDATED: {
        const { oldData, newData } = event.payload;
        const departmentId = newData._id;

        const oldStaffList = oldData.staffList;
        const oldCompanyId = oldData.companyId;
        const oldDepartmentTypeId = oldData.departmentTypeId;
        const oldDepartmentManagerUserId = oldData.departmentManagerUserId;

        const newStaffList = newData.staffList;
        const newCompanyId = newData.companyId;
        const newDepartmentTypeId = newData.departmentTypeId;
        const newDepartmentManagerUserId = newData.departmentManagerUserId;

        const subTaskList = [];
        const changedUserList = [];
        const oldDivisionList = [];
        const oldDivisionTypeList = [];

        oldStaffList.forEach((staff) => {
          changedUserList.push(staff.userId);
          oldDivisionList.push(staff.divisionId);
          oldDivisionTypeList.push(staff.divisionTypeId);
        });

        subTaskList.push((cb) => { // remove companyId from ALL user
          UserFeatureModel.updateMany(
            {
              userFeatureList: {
                $elemMatch: {
                  featureName: USER_FEATURE.COMPANY_LIST,
                },
              },

              userId: {
                $in: changedUserList,
              },
            },
            {
              $pull: {
                'userFeatureList.$.value': oldCompanyId,
              }
            },
            cb
          );
        });

        subTaskList.push((cb) => { // remove departmentId from ALL user
          UserFeatureModel.updateMany(
            {
              userFeatureList: {
                $elemMatch: {
                  featureName: USER_FEATURE.DEPARTMENT_LIST,
                },
              },

              userId: {
                $in: changedUserList,
              },
            },
            {
              $pull: {
                'userFeatureList.$.value': departmentId,
              }
            },
            cb
          );
        });

        subTaskList.push((cb) => { // remove departmentTypeId from ALL user
          UserFeatureModel.updateMany(
            {
              userFeatureList: {
                $elemMatch: {
                  featureName: USER_FEATURE.DEPARTMENT_TYPE_LIST,
                },
              },

              userId: {
                $in: changedUserList,
              },
            },

            {
              $pull: {
                'userFeatureList.$.value': oldDepartmentTypeId,
              }
            },
            cb
          );
        });

        _.uniq(oldDivisionList).forEach((divisionId) => {
          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DIVISION_LIST,
                  },
                },

                userId: {
                  $in: changedUserList,
                },
              },

              {
                $pull: {
                  'userFeatureList.$.value': divisionId,
                }
              },
              cb
            );
          });
        });

        _.uniq(oldDivisionTypeList).forEach((divisionTypeId) => {
          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DIVISION_TYPE_LIST,
                  },
                },

                userId: {
                  $in: changedUserList,
                },
              },

              {
                $pull: {
                  'userFeatureList.$.value': divisionTypeId,
                }
              },
              cb
            );
          });
        });

        if (oldDepartmentManagerUserId) {
          changedUserList.push(oldDepartmentManagerUserId);

          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId: oldDepartmentManagerUserId,

                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DEPARTMENT_LIST,
                  },
                }
              },
              {
                $pull: {
                  'userFeatureList.$.value': departmentId,
                }
              },
              cb,
            );
          });
        }

        if (newDepartmentManagerUserId) {
          changedUserList.push(newDepartmentManagerUserId);

          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId: newDepartmentManagerUserId,
                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DEPARTMENT_LIST,
                  },
                }
              },
              {
                $addToSet: {
                  'userFeatureList.$.value': departmentId,
                }
              },
              cb
            );
          });
        }

        newStaffList.forEach((staff) => {
          const { userId, divisionId, divisionTypeId, } = staff;

          changedUserList.push(userId);

          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId,

                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.COMPANY_LIST,
                  },
                }
              },

              {
                $addToSet: {
                  'userFeatureList.$.value': newCompanyId,
                }
              },
              cb
            );
          });

          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId,

                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DEPARTMENT_LIST,
                  },
                },
              },

              {
                $addToSet: {
                  'userFeatureList.$.value': departmentId,
                }
              },
              cb
            );
          });

          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId,

                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DEPARTMENT_TYPE_LIST,
                  },
                },
              },

              {
                $addToSet: {
                  'userFeatureList.$.value': newDepartmentTypeId,
                }
              },
              cb
            );
          });

          if (divisionId) {
            subTaskList.push((cb) => {
              UserFeatureModel.updateMany(
                {
                  userId,

                  userFeatureList: {
                    $elemMatch: {
                      featureName: USER_FEATURE.DIVISION_LIST,
                    },
                  },
                },

                {
                  $addToSet: {
                    'userFeatureList.$.value': divisionId,
                  }
                },
                cb
              );
            });

            subTaskList.push((cb) => {
              UserFeatureModel.updateMany(
                {
                  userId,

                  userFeatureList: {
                    $elemMatch: {
                      featureName: USER_FEATURE.DIVISION_TYPE_LIST,
                    },
                  },
                },

                {
                  $addToSet: {
                    'userFeatureList.$.value': divisionTypeId,
                  }
                },
                cb
              );
            });
          } // if (divisionId)
        });

        subTaskList.push((cb) => { // in the end, re-calc user related access list
          UserFeatureModel.find(
            {
              userId: {
                $in: _.uniq(changedUserList),
              }
            },

            async (error, userFeatureList) => {
              if (error) {
                cb(error);
              } else {
                if (_.isArray(userFeatureList)) {
                  userFeatureList.forEach(async (userFeature) => {
                    accessList = _.concat(accessList, await getUserRelatedAccessList(userFeature));
                  });

                  cb(null);
                }
              }
            }
          );
        });

        if (subTaskList.length > 0) {
          async.series(subTaskList, eventHandleCallBack);
        } else {
          eventHandleCallBack(null);
        }

        return;
      }

      case divisionEvent.CREATED: {
        const { newData } = event.payload;
        const divisionId = newData._id;

        const {
          companyId, staffList,
          departmentId, departmentTypeId,
          divisionTypeId
        } = newData;

        const subTaskList = [];

        staffList.forEach((staff) => {
          const { userId } = staff;

          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId,
                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.COMPANY_LIST,
                  },
                }
              },
              {
                $addToSet: {
                  'userFeatureList.$.value': companyId,
                }
              },
              cb
            );
          });

          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId,
                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DEPARTMENT_LIST,
                  },
                },
              },

              {
                $addToSet: {
                  'userFeatureList.$.value': departmentId,
                }
              },
              cb
            );
          });

          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId,
                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DEPARTMENT_TYPE_LIST,
                  },
                },
              },

              {
                $addToSet: {
                  'userFeatureList.$.value': departmentTypeId,
                }
              },
              cb
            );
          });

          if (divisionId) {
            subTaskList.push((cb) => {
              UserFeatureModel.updateMany(
                {
                  userId,
                  userFeatureList: {
                    $elemMatch: {
                      featureName: USER_FEATURE.DIVISION_LIST,
                    },
                  },
                },
                {
                  $addToSet: {
                    'userFeatureList.$.value': divisionId,
                  }
                },
                cb
              );
            });

            subTaskList.push((cb) => {
              UserFeatureModel.updateMany(
                {
                  userId,
                  userFeatureList: {
                    $elemMatch: {
                      featureName: USER_FEATURE.DIVISION_TYPE_LIST,
                    },
                  },
                },
                {
                  $addToSet: {
                    'userFeatureList.$.value': divisionTypeId,
                  }
                },
                cb
              );
            });
          } // if (divisionId)
        });

        if (subTaskList.length > 0) {
          async.series(subTaskList, eventHandleCallBack);
        } else {
          eventHandleCallBack(null);
        }

        return;
      }

      case divisionEvent.UPDATED: {
        const { oldData, newData } = event.payload;
        const divisionId = newData._id;

        const oldStaffList = oldData.staffList;
        const oldCompanyId = oldData.companyId;
        const oldDepartmentId = oldData.departmentId;
        const oldDepartmentTypeId = oldData.departmentTypeId;
        const oldDivisionManagerUserId = oldData.divisionManagerUserId;
        const oldDivisionTypeId = oldData.divisionTypeId;

        const newStaffList = newData.staffList;
        const newCompanyId = newData.companyId;
        const newDepartmentTypeId = newData.departmentTypeId;
        const newDepartmentId = newData.departmentId;
        const newDivisionManagerUserId = newData.divisionManagerUserId;
        const newDivisionTypeId = newData.divisionTypeId;

        const subTaskList = [];
        const changedUserList = [];

        oldStaffList.forEach((staff) => {
          changedUserList.push(staff.userId);
        });

        subTaskList.push((cb) => { // remove companyId from ALL user
          UserFeatureModel.updateMany(
            {
              userFeatureList: {
                $elemMatch: {
                  featureName: USER_FEATURE.COMPANY_LIST,
                },
              },

              userId: {
                $in: changedUserList,
              }
            },

            {
              $pull: {
                'userFeatureList.$.value': oldCompanyId,
              }
            },
            cb
          );
        });

        subTaskList.push((cb) => { // remove departmentId from ALL user
          UserFeatureModel.updateMany(
            {
              userFeatureList: {
                $elemMatch: {
                  featureName: USER_FEATURE.DEPARTMENT_LIST,
                },
              },

              userId: {
                $in: changedUserList,
              }
            },

            {
              $pull: {
                'userFeatureList.$.value': oldDepartmentId,
              }
            },
            cb
          );
        });

        subTaskList.push((cb) => { // remove departmentTypeId from ALL user
          UserFeatureModel.updateMany(
            {
              userFeatureList: {
                $elemMatch: {
                  featureName: USER_FEATURE.DEPARTMENT_TYPE_LIST,
                },
              },

              userId: {
                $in: changedUserList,
              }
            },

            {
              $pull: {
                'userFeatureList.$.value': oldDepartmentTypeId,
              }
            },
            cb
          );
        });

        if (oldDivisionManagerUserId) {
          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId: oldDivisionManagerUserId,

                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DEPARTMENT_LIST,
                  },
                }
              },

              {
                $pull: {
                  'userFeatureList.$.value': oldDepartmentId,
                }
              },
              cb,
            );
          });
        }

        if (newDivisionManagerUserId) {
          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId: newDivisionManagerUserId,

                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DEPARTMENT_LIST,
                  },
                }
              },

              {
                $addToSet: {
                  'userFeatureList.$.value': newDepartmentId,
                }
              },
              cb
            );
          });
        }

        subTaskList.push((cb) => {
          UserFeatureModel.updateMany(
            {
              userFeatureList: {
                $elemMatch: {
                  featureName: USER_FEATURE.DIVISION_LIST,
                },
              },

              userId: {
                $in: changedUserList,
              }
            },

            {
              $pull: {
                'userFeatureList.$.value': divisionId,
              }
            },
            cb
          );
        });

        subTaskList.push((cb) => {
          UserFeatureModel.updateMany(
            {
              userFeatureList: {
                $elemMatch: {
                  featureName: USER_FEATURE.DIVISION_TYPE_LIST,
                },
              },

              userId: {
                $in: changedUserList,
              }
            },

            {
              $pull: {
                'userFeatureList.$.value': oldDivisionTypeId,
              }
            },
            cb
          );
        });

        newStaffList.forEach((staff) => {
          const { userId } = staff;

          changedUserList.push(userId);

          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId,

                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.COMPANY_LIST,
                  },
                }
              },
              {
                $addToSet: {
                  'userFeatureList.$.value': newCompanyId,
                }
              },
              cb
            );
          });

          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId,

                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DEPARTMENT_LIST,
                  },
                },
              },

              {
                $addToSet: {
                  'userFeatureList.$.value': newDepartmentId,
                }
              },
              cb
            );
          });

          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId,

                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DEPARTMENT_TYPE_LIST,
                  },
                },
              },

              {
                $addToSet: {
                  'userFeatureList.$.value': newDepartmentTypeId,
                }
              },
              cb
            );
          });

          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId,

                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DIVISION_LIST,
                  },
                },
              },

              {
                $addToSet: {
                  'userFeatureList.$.value': divisionId,
                }
              },
              cb
            );
          });

          subTaskList.push((cb) => {
            UserFeatureModel.updateMany(
              {
                userId,

                userFeatureList: {
                  $elemMatch: {
                    featureName: USER_FEATURE.DIVISION_TYPE_LIST,
                  },
                },
              },

              {
                $addToSet: {
                  'userFeatureList.$.value': newDivisionTypeId,
                }
              },
              cb
            );
          });
        });

        subTaskList.push((cb) => { // in the end, re-calc user related access list
          UserFeatureModel.find(
            {
              userId: {
                $in: _.uniq(changedUserList),
              }
            },
            async (error, userFeatureList) => {
              if (error) {
                cb(error);
              } else {
                if (_.isArray(userFeatureList)) {
                  userFeatureList.forEach(async (userFeature) => {
                    accessList = _.concat(accessList, await getUserRelatedAccessList(userFeature));
                  });

                  cb(null);
                }
              }
            }
          );
        });

        if (subTaskList.length > 0) {
          async.series(subTaskList, eventHandleCallBack);
        } else {
          eventHandleCallBack(null);
        }

        return;
      }

      //send mail price
      case priceEvent.SEND_MAIL: {
        const newData = event.payload.newData;
        const DataModel = mongoose.model('roles');
        let emailList = [];
        let CC = [];
        let roleId = [];

        let listProductSegmentId = [];
        let listProductSegmentName = [];
        if (newData.productSegmentList.length > 0) {
          newData.productSegmentList.forEach(item => {
            listProductSegmentId.push(item.productSegmentId)
            listProductSegmentName.push(item.productSegmentName)
          });
        }
        let subject = '[Synnex FPT - PM] Duyệt bảng giá';
        let content = 'Dear Anh/Chị!<br /><br />'
        content += 'Bảng giá của ' + newData.createdByUserName + ' đang ở trạng thái';

        if (newData.state == SENT) {
          subject = '[Synnex FPT - PM] Duyệt bảng giá chờ PM duyệt';
          content += ' chờ PM duyệt.<br /><br />'

          let result = await DataModel.find({ roleCode: PM }, { _id: 1 });
          if (result.length > 0) {
            result.forEach(item => {
              roleId.push(item._id)
            })
          };

          let user = await UserFeatureModel.find({
            $and: [
              { 'userFeatureList.value': { $in: roleId.toString() } },
              { 'userFeatureList.value': { $in: listProductSegmentId } }
            ]
          }, { email: 1, _id: 0 })
          if (user.length > 0) {
            user.forEach(item => {
              if (item.email) {
                emailList.push(item.email);
              }
            });
            CC.push(newData.createdByUserName.concat('@fpt.com.vn'));
          }
        }

        if ((newData.state == PENDDING_BD) && (newData.penddingBD == true)) {
          subject = '[Synnex FPT - BD] Duyệt bảng giá chờ BD duyệt';
          content += ' chờ BD duyệt.<br /><br />'
          let result = await DataModel.find({ roleCode: BD }, { _id: 1 });

          if (result.length > 0) {
            result.forEach(item => {
              roleId.push(item._id)
            })
          };
          let user = await UserFeatureModel.find({
            $and: [
              { 'userFeatureList.value': { $in: roleId.toString() } },
              { 'userFeatureList.value': { $in: listProductSegmentId } }
            ]
          }, { email: 1, _id: 0 })
          if (user.length > 0) {
            user.forEach(item => {
              if (item.email) {
                emailList.push(item.email);
              }
            });
            CC.push(newData.createdByUserName.concat('@fpt.com.vn'));
          }
        }

        if ((newData.state == PENDDING_BOD) && (newData.penddingBOD == true)) {
          subject = '[Synnex FPT - BOD] Duyệt bảng giá chờ BOD duyệt';
          content += ' chờ BOD duyệt.<br /><br />'
          let result = await DataModel.find({ roleCode: BOD }, { _id: 1 });

          if (result.length > 0) {
            result.forEach(item => {
              roleId.push(item._id)
            })
          };

          let user = await UserFeatureModel.find({ 'userFeatureList.value': { $in: roleId.toString() } }, { email: 1, _id: 0 })

          if (user.length > 0) {
            user.forEach(item => {
              if (item.email) {
                emailList.push(item.email);
              }
            });
            CC.push(newData.createdByUserName.concat('@fpt.com.vn'));
          }
        }

        if (newData.state == APPROVED) {
          subject = 'Bảng giá đã được phê duyệt';
          content += ' đã được phê duyệt<br /><br />';
          emailList.push(newData.createdByUserName.concat('@fpt.com.vn'));
        }

        if (newData.state == CANCLED) {
          subject = 'Bảng giá đã không được phê duyệt và bị hủy';
          content += ' đã không được phê duyệt và bị hủy<br /><br />'
          emailList.push(newData.createdByUserName.concat('@fpt.com.vn'));
        }

        content += 'Nội dung: ' + newData.content + '<br/>';
        content += 'Dòng sản phẩm: ' + listProductSegmentName + '<br/>';
        content += 'Người tạo: ' + newData.createdByUserName + '<br/><br/>';
        content += 'Để biết thêm thông tin chi tiết, anh chị vui lòng đăng nhập hệ thống Insight và <a href="' + domain + 'pm/prices/' + newData._id + '">click vào đây</a>';

        const sendTo = emailList.join(",").toString();

        if (sendTo != '') {
          sendEmail(subject, content, sendTo, CC);
        }
        break;
      }

      // send mail marrketing
      case marketingEvent.SENTSMC: {
        const data = event.payload.newData;
        const emailList = [];

        const DataModel = mongoose.model('roles');
        var roleId = [];

        if (data.campaignTypeCode === "CHH") {
          let result = await DataModel.find({ roleCode: BD }, { _id: 1 });

          if (result.length > 0) {
            result.forEach(item => {
              roleId.push(item._id)
            })
          };

          let user = await UserFeatureModel.find({
            $and: [
              { 'userFeatureList.value': { $in: roleId.toString() } },
              { 'userFeatureList.value': { $in: data.productSegmentList } }
            ]
          }, { email: 1, _id: 0 })

          if (user.length > 0) {
            user.forEach(item => {
              if (item.email) {
                emailList.push(item.email);
              }
            });
          }
        } else {
          let result = await DataModel.find({ roleCode: SMC }, { _id: 1 });

          if (result.length > 0) {
            result.forEach(item => {
              roleId.push(item._id)
            })
          };

          let user = await UserFeatureModel.find({ 'userFeatureList.value': { $in: roleId.toString() } }, { email: 1, _id: 0 })

          if (user.length > 0) {
            user.forEach(item => {
              if (item.email) {
                emailList.push(item.email);
              }
            });
          }
        }
        let content = 'Dear anh/chị,<br /><br />';
        if (data.campaignTypeCode === "CHH") {
          var subject = 'Chờ BD duyệt chương trình marketing';
          content += ' Anh chị vui lòng vào duyệt chương trình Marketing để phục vụ cho công việc. Thông tin chương trình Marketing:';
        } else {
          var subject = 'Chờ SMC duyệt yêu cầu marketing';
          content += ' Anh chị vui lòng vào duyệt yêu cầu Marketing để phục vụ cho công việc. Thông tin yêu cầu Marketing:';
        }

        data.createdAt = new Date();
        content += '<br /><br />';
        content += 'Người tạo: ' + data.createdByUserName + '<br />';
        content += 'Ngày tạo: ' + (`${date.format(new Date(), 'DD/MM/YYYY ')}`) + '<br />';
        content += 'Mã yêu cầu tạo chương Trình: ' + data.marketingCampaignCode + '<br />';
        content += 'Tên yêu cầu: ' + data.marketingCampaignName + '<br />';
        content += 'Dòng sản phẩm: ' + data.productSegmentNameList + '<br />';

        if (data.campaignTypeCode === "CHH") {
          content += 'Để biết thêm thông tin chi tiết, anh chị vui lòng đăng nhập hệ thống Insight và <a href="' + domain + 'pm/approveMarketings/' + data._id + '">click vào đây</a>';
        } else {
          content += 'Để biết thêm thông tin chi tiết, anh chị vui lòng đăng nhập hệ thống Insight và <a href="' + domain + 'pm/marketings/' + data._id + '">click vào đây</a>';
        }

        var sendTo = emailList;
        if (sendTo.length > 0) {
          sendEmail(subject, content, sendTo);
        }

        break;
      }

      case marketingEvent.SMCSENT: {
        const data = event.payload.newData;
        const emailList = [];

        const DataModel = mongoose.model('roles');
        var roleId = [];
        let result = await DataModel.find({ roleCode: BD }, { _id: 1 });

        if (result.length > 0) {
          result.forEach(item => {
            roleId.push(item._id)
          })
        };

        let user = await UserFeatureModel.find({
          $and: [
            { 'userFeatureList.value': { $in: roleId.toString() } },
            { 'userFeatureList.value': { $in: data.productSegmentList } }
          ]
        }, { email: 1, _id: 0 })
        if (user.length > 0) {
          user.forEach(item => {
            if (item.email) {
              emailList.push(item.email);
            }
          });
        }

        const subject = 'Chờ BD duyệt chương trình marketing';
        let content = 'Dear anh/chị,<br /><br />';
        content += ' Anh chị vui lòng vào duyệt Chương Trình Marketing để phục vụ cho công việc. Thông tin chương trình Marketing:';
        content += '<br /><br />';
        content += 'Người tạo: ' + data.createdByUserName + '<br />';
        content += 'Ngày tạo: ' + (`${date.format(new Date(data.createdAt), 'DD/MM/YYYY ')}`) + '<br />';
        content += 'Mã chương trình: ' + data.marketingCode + '<br />';
        content += 'Tên truyền thông: ' + data.mediaName + '<br />';
        content += 'Dòng sản phẩm: ' + data.productSegmentNameList + '<br />';
        content += 'Để biết thêm thông tin chi tiết, anh chị vui lòng đăng nhập hệ thống Insight và <a href="' + domain + 'pm/approveMarketings/' + data._id + '">click vào đây</a>';
        const sendTo = emailList.join(",").toString();
        if (sendTo != '') {
          sendEmail(subject, content, sendTo);
        }

        break;
      }

      case marketingEvent.APPROVED: {
        const data = event.payload.newData;

        const emailList = [];
        let CC = [];
        let content = 'Dear anh/chị,<br /><br />';
        let subject = '';
        const DataModel = mongoose.model('roles');
        var roleId = [];

        if ((data.fundingSources === VMDF || data.fundingSources === VJFD || data.fundingSources === COOP) && (data.marketingState === 'var')) {

          let result = await DataModel.find({ roleCode: VAR }, { _id: 1 });

          if (result.length > 0) {
            result.forEach(item => {
              roleId.push(item._id)
            })
          };

          let user = await UserFeatureModel.find({ 'userFeatureList.value': { $in: roleId.toString() } }, { email: 1, _id: 0 })

          if (user.length > 0) {
            user.forEach(item => {
              if (item.email) {
                emailList.push(item.email);
              }
            });
          }

          subject += 'Chờ VAR duyệt chương trình marketing';
          content += ' Anh chị vui lòng vào duyệt Chương Trình Marketing để phục vụ cho công việc. Thông tin chương trình Marketing:';
          content += '<br /><br />';
          content += 'Người tạo: ' + data.createdByUserName + '<br />';
          content += 'Ngày tạo: ' + (`${date.format(new Date(data.createdAt), 'DD/MM/YYYY ')}`) + '<br />';
          content += 'Mã chương trình: ' + data.marketingCode + '<br />';
          content += 'Tên truyền thông: ' + data.mediaName + '<br />';
          content += 'Dòng sản phẩm: ' + data.productSegmentNameList + '<br />';
        }
        else if (data.marketingState === 'ktt') {
          let result = await DataModel.find({ roleCode: FAF }, { _id: 1 });

          if (result.length > 0) {
            result.forEach(item => {
              roleId.push(item._id)
            })
          };
          let user = await UserFeatureModel.find({ 'userFeatureList.value': { $in: roleId.toString() } }, { email: 1, _id: 0 })

          if (user.length > 0) {
            user.forEach(item => {
              if (item.email) {
                emailList.push(item.email);
              }
            });
          }

          subject += 'Chờ FAF duyệt chương trình marketing';
          content += ' Anh chị vui lòng vào duyệt Chương Trình Marketing để phục vụ cho công việc. Thông tin chương trình Marketing:';
          content += '<br /><br />';
          content += 'Người tạo: ' + data.createdByUserName + '<br />';
          content += 'Ngày tạo: ' + (`${date.format(new Date(data.createdAt), 'DD/MM/YYYY ')}`) + '<br />';
          content += 'Mã chương trình: ' + data.marketingCode + '<br />';
          content += 'Tên truyền thông: ' + data.mediaName + '<br />';
          content += 'Dòng sản phẩm: ' + data.productSegmentNameList + '<br />';
        }
        else if (data.marketingState === 'bod') {
          let result = await DataModel.find({ roleCode: BOD }, { _id: 1 });

          if (result.length > 0) {
            result.forEach(item => {
              roleId.push(item._id)
            })
          };

          let user = await UserFeatureModel.find({ 'userFeatureList.value': { $in: roleId.toString() } }, { email: 1, _id: 0 })

          if (user.length > 0) {
            user.forEach(item => {
              if (item.email) {
                emailList.push(item.email);
              }
            });
          }
          subject += 'Chờ BOD duyệt chương trình marketing';
          content += ' Anh chị vui lòng vào duyệt Chương Trình Marketing để phục vụ cho công việc. Thông tin chương trình Marketing:';
          content += '<br /><br />';
          content += 'Người tạo: ' + data.createdByUserName + '<br />';
          content += 'Ngày tạo: ' + (`${date.format(new Date(data.createdAt), 'DD/MM/YYYY ')}`) + '<br />';
          content += 'Mã chương trình: ' + data.marketingCode + '<br />';
          content += 'Tên truyền thông: ' + data.mediaName + '<br />';
          content += 'Dòng sản phẩm: ' + data.productSegmentNameList + '<br />';
        }
        else if (data.marketingState === 'approved') {

          let result = await DataModel.find({ roleCode: SMC_MANAGER }, { _id: 1 });

          if (result.length > 0) {
            result.forEach(item => {
              roleId.push(item._id)
            })
          };

          let user = await UserFeatureModel.find({ 'userFeatureList.value': { $in: roleId.toString() } }, { email: 1, _id: 0 })

          if (user.length > 0) {
            user.forEach(item => {
              if (item.email) {
                emailList.push(item.email);
              }
            });
          }
          emailList.push(data.createdByUserName.concat('.@fpt.com.vn'));
          CC.push(data.orderCreatedByUserName.concat('.@fpt.com.vn'));

          subject += 'Chương trình marketing đã được phê duyệt';
          content += ' Thông tin chương trình Marketing:'
          content += '<br />';
          content += 'Người tạo: ' + data.createdByUserName + '<br />';
          content += 'Ngày tạo: ' + (`${date.format(new Date(data.createdAt), 'DD/MM/YYYY ')}`) + '<br />';
          content += 'Mã chương trình: ' + data.marketingCode + '<br />';
          content += 'Tên truyền thông: ' + data.mediaName + '<br />';
          content += 'Dòng sản phẩm: ' + data.productSegmentNameList + '<br />';
        }
        else if (data.marketingState === 'draft') {
          subject += 'Chương trình marketing đã bị từ chối phê duyệt';
          content += ' Anh chị vui lòng vào "chỉnh sửa/ bổ sung Chương Trình Marketing để phục vụ cho công việc.' + '<br />';
          content += '<br /><br />';
          content += 'Người tạo: ' + data.createdByUserName + '<br />';
          content += 'Ngày tạo: ' + (`${date.format(new Date(data.createdAt), 'DD/MM/YYYY ')}`) + '<br />';
          content += 'Mã chương trình: ' + data.marketingCode + '<br />';
          content += 'Tên truyền thông: ' + data.mediaName + '<br />';
          content += 'Dòng sản phẩm: ' + data.productSegmentNameList + '<br />';

          let result = await DataModel.find({ roleCode: { $in: [SMC, SMC_MANAGER] } }, { _id: 1 });

          if (result.length > 0) {
            result.forEach(item => {
              roleId.push(item._id.toString())
            })
          };

          let user = await UserFeatureModel.find({ 'userFeatureList.value': { $in: roleId } }, { email: 1, _id: 0 });
          if (user.length > 0) {
            user.forEach(item => {
              if (item.email) {
                emailList.push(item.email);
              }
            });
          }
          CC.push(data.orderCreatedByUserName.concat('.@fpt.com.vn'));
        }
        if (data.marketingState === 'approved') {
          content += 'Để biết thêm thông tin chi tiết, anh chị vui lòng đăng nhập hệ thống Insight và <a href="' + domain + 'pm/marketings/' + data._id + '">click vào đây</a>';
        } else {
          content += 'Để biết thêm thông tin chi tiết, anh chị vui lòng đăng nhập hệ thống Insight và <a href="' + domain + 'pm/approveMarketings/' + data._id + '">click vào đây</a>';
        }

        const sendTo = emailList;

        if (sendTo.length > 0) {
          sendEmail(subject, content, sendTo, CC);

          break;
        }
      }
      default: {
        break;
      }
    }

    if (accessList.length > 0) {
      await AccessList.insertMany(accessList);
    }

    eventHandleCallBack(null);
  } catch (error) {
    eventHandleCallBack(error);
  }
};
