import mongoose from 'mongoose';
import async from 'async';
import userEvent from '../events/userEvent';

export const model = 'esdSalesteams';

export const servedEvent = [
  userEvent.UPDATED,
];

export const handle = (event, eventHandleCallBack) => {
  try {
    const DataModel = mongoose.model(model);
    const taskList = [];

    switch (event.type) {
      case userEvent.UPDATED: {
        const user = event.payload.newData;

        taskList.push((cb) => {
          DataModel.update(
            { createdBy: user._id },
            {
              createdByUserName: user.userName,
              createdByFullName: user.fullName,
            },
            { multi: true },
            cb,
          );
        });

        taskList.push((cb) => {
          DataModel.update(
            { updatedBy: user._id },
            {
              updatedByUserName: user.userName,
              updatedByFullName: user.fullName,
            },
            { multi: true },
            cb,
          );
        });

        break;
      }

      default: {
        break;
      }
    } // switch (event.type)

    if (taskList.length > 0) {
      async.series(taskList, eventHandleCallBack);
    } else {
      eventHandleCallBack(null);
    }
  } catch (error) {
    eventHandleCallBack(error);
  }
};
