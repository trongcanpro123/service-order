import mongoose from 'mongoose';
import async from 'async';
import userEvent from '../events/userEvent';
import esdCustomerEvent from '../events/esdCustomerEvent';
import esdOrderEvent from '../events/esdOrderEvent';
import esdPurchaseOrderEvent from '../events/esdPurchaseOrderEvent';
import smtp from '../../constants/smtpMailConstant';
const nodemailer = require('nodemailer');

export const model = 'esdOrders';

export const servedEvent = [
  userEvent.UPDATED,
  // esdCustomerEvent.UPDATED,
  esdOrderEvent.UPDATED,
  esdOrderEvent.CREATED,
  esdOrderEvent.DONE,
  esdOrderEvent.PROCESS,
  esdPurchaseOrderEvent.CREATED,
];

export const handle = (event, eventHandleCallBack) => {
  try {
    const DataModel = mongoose.model(model);
    const EsdCustomerModel = mongoose.model('esdCustomers');
    const UserModel = mongoose.model('users');
    const taskList = [];

    switch (event.type) {
      case esdPurchaseOrderEvent.CREATED:
      {
        // filter userIdList from customerId
        // update userIdList to esdOrders with _id
        const esdOrder = event.payload.newData;
        // console.log(esdOrder);
        taskList.push((cb) => {
          EsdCustomerModel.find(
            { _id: esdOrder.customerId},
            {
              userIdList: true
            },
            function (err, data) {
              console.log('entry_vh', data[0].userIdList);
              var userIdList = data[0].userIdList;
              DataModel.update(
                { customerId: esdOrder.customerId },
                {
                  userIdList: userIdList
                },
                { multi: true },
                cb,
              );
            }
          );
        });

        break;
      }

      case userEvent.UPDATED: {
        const user = event.payload.newData;

        taskList.push((cb) => {
          DataModel.update(
            { createdBy: user._id },
            {
              createdByUserName: user.userName,
              createdByFullName: user.fullName,
            },
            { multi: true },
            cb,
          );
        });

        taskList.push((cb) => {
          DataModel.update(
            { updatedBy: user._id },
            {
              updatedByUserName: user.userName,
              updatedByFullName: user.fullName,
            },
            { multi: true },
            cb,
          );
        });

        break;
      }

      default: {
        break;
      }
    } // switch (event.type)

    if (taskList.length > 0) {
      async.series(taskList, eventHandleCallBack);
    } else {
      eventHandleCallBack(null);
    }
  } catch (error) {
    eventHandleCallBack(error);
  }
};
