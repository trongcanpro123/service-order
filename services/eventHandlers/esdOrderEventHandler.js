import mongoose from 'mongoose';
import config from 'config';
import async from 'async';
import axios from 'axios';
import sql from 'mssql';
import userEvent from '../events/userEvent';
import esdCustomerEvent from '../events/esdCustomerEvent';
import esdProductEvent from '../events/esdProductEvent';
import esdOrderEvent from '../events/esdOrderEvent';
import esdPurchaseOrderEvent from '../events/esdPurchaseOrderEvent';
import smtp from '../../constants/smtpMailConstant';
const nodemailer = require('nodemailer');
import sb from '../../helpers/serviceBusHelper';
import debug from 'debug';
const serviceBusDebugger = debug('app:serviceBus');
import { getEvent } from '../../helpers/eventHelper';
import { commonEvent } from '../../constants/eventConstant';
import { ORDER_NUMBER_PATTERN_API, ORDER_NUMBER_LENGTH } from '../constants/orderConstant';
import { getSequenceCode, TOKEN } from '../../helpers/commonHelper';
import { getLicenceFromMS } from '../functions/esdOrderFunction';

export const model = 'esdOrders';

export const servedEvent = [
  userEvent.UPDATED,
  esdCustomerEvent.UPDATED,
  esdOrderEvent.UPDATED,
  esdOrderEvent.CREATED,
  esdOrderEvent.CREATED_BY_API,
  esdOrderEvent.CREATED_BY_API_V2,
  esdOrderEvent.DONE,
  esdOrderEvent.PROCESS,
  esdProductEvent.COMBINE_PURCHASE,
];

export const handle = async (event, eventHandleCallBack) => {
  try {
    const DataModel = mongoose.model(model);
    const EsdCustomerModel = mongoose.model('esdCustomers');
    const UserModel = mongoose.model('users');
    const taskList = [];

    switch (event.type) {
      case esdProductEvent.COMBINE_PURCHASE:  {
        const data = event.payload.newData;
        // console.log('data', data);
        const dataObject = new DataModel();

        Object.entries(data).forEach(([key, value]) => {
          if (key !== '_id') {
            // console.log('key', key);
            dataObject[key] = value;
            // console.log('dataObject', dataObject);
          }
        });

        const permissionList = [];
        permissionList.push({
          userId: data.createdBy,
          userName: data.createdByUserName,
          fullName: data.createdByFullName,

          canRead: true,
          canUpdate: true,
          canDelete: true,
        });
        dataObject['permissionList'] = permissionList;
        // console.log('dataObject', dataObject);
        dataObject['orderState'] = 'sent';
        dataObject['purchaseState'] = 'entered';
        dataObject['orderStateSM'] = 'entered';
        dataObject['status'] = 'loaded';
        dataObject['orderNumber'] = await getSequenceCode(ORDER_NUMBER_PATTERN_API, ORDER_NUMBER_LENGTH);
        dataObject.save().then((data2) => {
          // const eventType = getEvent(model, commonEvent.CREATED_BY_API);
          const eventType = getEvent(model, commonEvent.CREATED_BY_API_V2);
      
          sb.publish(model, eventType, { model, oldData: {}, newData: data2 }, '0', (err) => {
            if (err) {
              serviceBusDebugger(`[SB] Publish error: ${colors.red(err)}`);
            }
          });
        });

        break;
      }

      case esdOrderEvent.CREATED_BY_API_V2: {
        const data = event.payload.newData;
        const { purchaseOrderNumber, customerId, customerOracleCode, customerName } = data;
        let total = 0;
        for (const order of data.orderLineAll) {
          total += order.unitPrice * order.quantity;
          const request = {
            purchaseOrderNumber,
            customerId,
            customerOracleCode,
            customerName,
            quantity: order.quantity,
            query: {
              // ctid: uuid.v4(),
              sku: order.partNumber,
              resellerId: data.customerOracleCode,
              po: await getSequenceCode(`POAPI${data.customerOracleCode}${TOKEN.SEPARATOR}${TOKEN.SEQUENCE}`, 4, "-"),
              storeId: 'StoreFTG',
              countryCode: 'VN'
            }
          }
          const licence = await getLicenceFromMS(request);
          DataModel.findOneAndUpdate({
            _id: data._id,
            'orderLineAll.lineId': order.lineId
          },
          {
            $set: {
              'orderLineAll.$.licence': licence,
            }
          },
          {new: true},
          (err, data1) => {
            if(err){
              console.log('err 1', err);
            } else {
              // console.log(`import licence ${order.lineId}`, data1);
              // console.log('hungnv104 -> data1.orderLineAll:', order.lineId, data1.orderLineAll);
              const lic = data1.orderLineAll.find(s => s.lineId == order.lineId);
              console.log('hungnv104 -> lic.licence:', lic.licence);
              if (lic.licence.length === order.quantity) {
                DataModel.update({
                  _id: data._id,
                  'orderLineAll.lineId': order.lineId
                },
                {
                  $set: {
                    'orderLineAll.$.state': 'completed',
                  }
                },
                (err, data1) => {
                  if(err){
                    console.log('err 1', err);
                  } else {
                    DataModel.findOne(
                      {
                        '_id': data._id,
                        'orderLineAll.state': 'pendingLicence'
                      },
                      {
                        'orderLineAll.state': 1
                      },
                      (err2, data2) => {
                        if (err2) {
                          console.log('err2', err2);
                        } else {
                          console.log('data2', data2);
                          if (data2 == null) {
                            DataModel.updateMany(
                              {
                                '_id': data._id,
                              },
                              {
                                $set:
                                {
                                  purchaseState: 'completed',
                                }
                              },
                              (err2, data2) => {
                                if (err2) {
                                  console.log('err2', err2);
                                } else {
                                  console.log('data2', data2);
                                }
                              }
                            );
                          }
                        }
                      }
                    );
                  }
                })
              }
            }
          })
        }

        DataModel.update({
          _id: data._id
        },
        {
          $set: {
            total,
            note: 'Đơn hàng API',
          }
        },
        (err, data2) => {
          if(err){
            console.log('err 1', err);
          } else {
            console.log(`update total, note for ${data.purchaseOrderNumber}`, data2);
          }
        })

        break;
      }

      case esdOrderEvent.CREATED_BY_API: {
        const data = event.payload.newData;
        console.log('dataNew', data);
        DataModel.update({
          _id: data._id
        },
        {
          $set: {
            note: 'Đơn hàng API'
          }
        },
        (err, data) => {
          if(err){
            console.log('err 1', err);
          } else {
            console.log('data 1', data);
          }
        })
        
        const start = async () => {
          const orderLineAll = [];
          let total = 0;
          for (const order of data.orderLineAll) {
            orderLineAll.push({
              "itemCode": order.oracleCode,
              "itemName": order.productName,
              "Sku": order.partNumber,
              "quantity": order.quantity,
              "tax": "OP-KHONG CHIU THUE",
              "price": order.unitPrice,
              "priceAddTax": "0",
              "lineId": order.lineId
            });
            total += order.unitPrice * order.quantity;
          }
          const poNumber = await getSequenceCode(`POAPI${data.customerOracleCode}${TOKEN.SEPARATOR}${TOKEN.SEQUENCE}`, 4, "-")
          const salesOrder = await getSequenceCode(`SOAPI${data.customerOracleCode}${TOKEN.SEPARATOR}${TOKEN.SEQUENCE}`, 4, "-")
          const dataTranfer = {
            "combinedCode": data.companyOracleCode + data.departmentOracleCode,
            "customerCode": data.customerOracleCode,
            "poNumber": poNumber,
            "salesOrder": salesOrder,
            "eInvoice": "",
            "salesman": data.salesmanName,
            "taxAmount": "0",
            "total": total,
            "orderItems": orderLineAll
          }
          
          console.log('dataTranfer', dataTranfer);
          const hostMS = process.env.HOST_ESD_MSLICENCE || config.get('hostEsdMSLicence');
          console.log('hungnv104 -> hostMS:', hostMS)
          axios({
            method: 'post',
            url: `${hostMS}/api/orders/create-order-from-sm`,
            data: dataTranfer,
            headers: {
              'Content-type': 'application/json; charset=UTF-8',
              'Accept': 'text/plain',
            },
          }).then((res) => {
            console.log('hungnv104 -> res:', res)
            // console.log('resESD', res);
            // update purchaseState
            // update orderId from ESD 205
            const config = {
              user: process.env.USER_ESD_MSLICENCE || config.get('userEsdMSLicence'),
              password: process.env.PASSWORD_ESD_MSLICENCE || config.get('passwordEsdMSLicence'),
              server: process.env.SERVER_ESD_MSLICENCE || config.get('serverEsdMSLicence'),
              database: process.env.DATABASE_ESD_MSLICENCE || config.get('databaseEsdMSLicence')
            };
            console.log('sql', `select Id from Orders_Order where PoNumber = ${poNumber} and SalesOrder = ${salesOrder}`);
            new sql.ConnectionPool(config).connect().then(pool => {
              return pool.query`select Id from Orders_Order where PoNumber = ${poNumber} and SalesOrder = ${salesOrder}`
            }).then(result => {
              let rows = result.recordset
              if (rows.length == 0) {
                console.log('rowsEsd', rows);
                DataModel.update({
                  _id: data._id
                },
                {
                  $set: {
                    total: total
                  }
                },
                (err, data) => {
                  if(err){
                    console.log('err 2', err);
                  } else {
                    console.log('data 2', data);
                  }
                })
              } else {
                console.log('rowsEsd', rows);
                DataModel.update({
                  _id: data._id
                },
                {
                  $set: {
                    total: total,
                    purchaseState: 'pendingLicence',
                    orderIdEsdMS: rows[0].Id
                  }
                },
                (err, data) => {
                  if(err){
                    console.log('err 2', err);
                  } else {
                    console.log('data 2', data);
                  }
                })
              }
              sql.close();
            }).catch(err => {
              sql.close();
            });

          }).catch((error) => {
            console.log('resESD_error', error.message);
          });
        }

        start();

        break;
      }

      case esdCustomerEvent.UPDATED: {
        // get userIdList from esdCustomer where customerId (1)
        // get all esdOrder where customerId
        // set userIdList(esdOrder) = userIdList (1)
        const esdCustomer = event.payload.newData;
        
        taskList.push((cb) => {
          EsdCustomerModel.find(
            { _id: esdCustomer._id},
            {
              userIdList: true
            },
            function (err, data) {
              console.log('entry_vh', data[0].userIdList);
              var userIdList = data[0].userIdList;
              DataModel.update(
                { customerId: esdCustomer._id },
                {
                  userIdList: userIdList
                },
                { multi: true },
                cb,
              );
            }
          );
        });

        break;
      }

      

      case esdOrderEvent.DONE: {
        const data = event.payload.newData;
        // console.log('data', data);
        taskList.push(async (cb) => {
          let obj;
          obj = await UserModel.find({"_id": data.permissionList[0].userId});
          const userDetail = obj[0];

          let transporter = nodemailer.createTransport({
            host: smtp.HOST,
            port: smtp.PORT,
            secure: smtp.SECURE,
            requireTLS: smtp.REQUIRETLS,
            auth: {
              user: smtp.USER,
              pass: smtp.PASS
            },
            tls: {
              rejectUnauthorized: false
            }
          });
          
          var content = '';
          content += 'Xin chào ' + userDetail.fullName + ',<br /><br />';
          content += 'Đã có licence cho đơn đặt hàng ' + data.orderNumber + '. Bạn vui lòng vào hệ thống https://portal.ftg.vn/esd/orders/'+ data._id +' để tải xuống licence.<br /><br />';
          content += 'Cảm ơn bạn đã tin tưởng và lựa chọn Synnex FPT là đối tác tin cậy và cùng đồng hành suốt thời gian qua!<br /><br />';
          
          let mailOptions = {
            from: 'Synnex FPT ' + smtp.USER,
            // to: 'nguyenhung537@gmail.com',
            to: userDetail.email,
            subject: '[Synnex FPT] Đơn hàng [' + data.orderNumber + '] đã hoàn thành ',
            html: content
          };
        
          transporter.sendMail(mailOptions, cb);

        });
        break;
      }
      
      case esdOrderEvent.PROCESS: {
        const data = event.payload.newData;
        // console.log('data', data);
        taskList.push(async (cb) => {
          let obj;
          obj = await UserModel.find({"_id": data.permissionList[0].userId});
          const userDetail = obj[0];
          console.log(userDetail);
          let transporter = nodemailer.createTransport({
            host: smtp.HOST,
            port: smtp.PORT,
            secure: smtp.SECURE,
            requireTLS: smtp.REQUIRETLS,
            auth: {
              user: smtp.USER,
              pass: smtp.PASS
            },
            tls: {
              rejectUnauthorized: false
            }
          });
          
          var content = '';
          content += 'Xin chào ' + userDetail.fullName + ',<br /><br />';
          content += 'Đơn đặt hàng ' + data.orderNumber + ' đã được chúng tôi đang xử lí duyệt đơn hàng. Bạn vui lòng vào hệ thống https://portal.ftg.vn/esd/orders/'+ data._id +' để theo dõi. Chúng tôi sẽ gửi lại cho bạn email mới khi đơn hàng được duyệt hoàn thành.<br /><br />';
          content += 'Cảm ơn bạn đã tin tưởng và lựa chọn Synnex FPT là đối tác tin cậy và cùng đồng hành suốt thời gian qua!<br /><br />';
          
          let mailOptions = {
            from: 'Synnex FPT ' + smtp.USER,
            // to: 'nguyenhung537@gmail.com',
            to: userDetail.email,
            subject: '[Synnex FPT] Đơn hàng [' + data.orderNumber + '] đang được xử lí',
            html: content
          };
        
          // transporter.sendMail(mailOptions, (cb) => {
          //   console.log(cb);
          // });

          transporter.sendMail(mailOptions, function(error, info){
            if(error){
                // console.log("/sendmail error");
                console.log(error);
                // res.sendStatus(500);
                // return;
            }else{
                console.log("Message sent: " + info.response);
                // if you don't want to use this transport object anymore, uncomment following line
                socketTimeout: 30 * 1000 // 0.5 min: Time of inactivity until the connection is closed
                transporter.close(); // shut down the connection pool, no more messages
                // res.sendStatus(200);
            }
    
            // if you don't want to use this transport object anymore, uncomment following line
            transporter.close(); // shut down the connection pool, no more messages
          });

        });
        break;
      }
      
      case esdOrderEvent.CREATED: 
      case esdOrderEvent.UPDATED: 
      {
        // filter userIdList from customerId
        // update userIdList to esdOrders with _id
        const esdOrder = event.payload.newData;
        // console.log(esdOrder);
        taskList.push((cb) => {
          EsdCustomerModel.find(
            { _id: esdOrder.customerId},
            {
              userIdList: true
            },
            function (err, data) {
              console.log('entry_vh', data[0].userIdList);
              var userIdList = data[0].userIdList;
              DataModel.update(
                { customerId: esdOrder.customerId },
                {
                  userIdList: userIdList
                },
                { multi: true },
                cb,
              );
            }
          );
        });

        break;
      }

      case userEvent.UPDATED: {
        const user = event.payload.newData;

        taskList.push((cb) => {
          DataModel.update(
            { createdBy: user._id },
            {
              createdByUserName: user.userName,
              createdByFullName: user.fullName,
            },
            { multi: true },
            cb,
          );
        });

        taskList.push((cb) => {
          DataModel.update(
            { updatedBy: user._id },
            {
              updatedByUserName: user.userName,
              updatedByFullName: user.fullName,
            },
            { multi: true },
            cb,
          );
        });

        break;
      }

      default: {
        break;
      }
    } // switch (event.type)

    if (taskList.length > 0) {
      async.series(taskList, eventHandleCallBack);
    } else {
      eventHandleCallBack(null);
    }
  } catch (error) {
    eventHandleCallBack(error);
  }
};
