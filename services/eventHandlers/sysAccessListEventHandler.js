import mongoose from 'mongoose';
import _ from 'lodash';

import policyEvent from '../events/policyEvent';
import { getUserAccessList } from '../../helpers/policyHelper';
import { serviceList } from '../config';
import { USER_FEATURE } from '../../constants/policyConstant';

export const model = 'sysAccessList';

export const servedEvent = [
  policyEvent.CREATED, // policy create => NO NEED update access list (cause of OR rule)
  policyEvent.UPDATED, // policy update => update all (user / function / action related) access list 
  policyEvent.DELETED, // policy update => update all (user / function / action related) access list 
];

const AccessList = mongoose.model(model);
const UserFeatureModel = mongoose.model('sysUserFeatures');
const PolicyModel = mongoose.model('sysPolicies');

export const handle = async (event, eventHandleCallBack) => {
  try {
    let accessList = [];
    let policyId;

    switch (event.type) {
      case policyEvent.CREATED: {
        const policy = event.payload.newData;
        const functionId = policy.functionId.toString();

        if (serviceList.indexOf(`/${policy.serviceCode}`) > -1) { // v1/departments => /v1/departments
          const dataObject = new PolicyModel();

          Object.entries(policy).forEach(([key, value]) => {
            dataObject[key] = value; //clone all (including "_id")
          });
  
          await dataObject.save();
  
          const userList = await UserFeatureModel.find({
            userFeatureList: {
              $elemMatch: {
                featureName: USER_FEATURE.FUNCTION_LIST,
                value: functionId,
              },
            },
          });

          if (_.isArray(userList)) {
            userList.forEach((user) => {
              accessList = _.concat(accessList, getUserAccessList(user, policy));
            });
          }
        }

        break;
      }
      
      case policyEvent.UPDATED: {
        const policy2 = event.payload.newData;
        const functionId2 = policy2.functionId.toString();

        if (serviceList.indexOf(`/${policy2.serviceCode}`) > -1) { // v1/departments => /v1/departments
          policyId = policy2._id;

          await PolicyModel.findByIdAndDelete(policyId);
          await AccessList.deleteMany({ policyId });

          const dataObject = new PolicyModel();

          Object.entries(policy2).forEach(([key, value]) => {
            dataObject[key] = value;
          });

          await dataObject.save();

          const userList2 = await UserFeatureModel.find({
            userFeatureList: {
              $elemMatch: {
                featureName: USER_FEATURE.FUNCTION_LIST,
                value: functionId2,
              },
            },
          });

          if (_.isArray(userList2)) {
            userList2.forEach((user) => {
              accessList = _.concat(accessList, getUserAccessList(user, policy2));
            });
          }
        }

        break;
      }

      case policyEvent.DELETED: {
        const policy3 = event.payload.newData;

        if (serviceList.indexOf(`/${policy3.serviceCode}`) > -1) {
          policyId = policy3._id;

          if (policyId) {
            await PolicyModel.findByIdAndDelete(policyId);
            await AccessList.deleteMany({ policyId });
          }
        }

        break;
      }

      default: {
        break;
      }
    }

    if (accessList.length > 0) {
      const rs = await AccessList.collection.insertMany(accessList);
    }

    eventHandleCallBack(null);
  } catch (error) {
    eventHandleCallBack(error);
  }
};
