import mongoose from 'mongoose';
import async from 'async';
import userEvent from '../events/userEvent';
import esdCustomerEvent from '../events/esdCustomerEvent';
import esdPurchaseOrderEvent from '../events/esdPurchaseOrderEvent';
import sb from '../../helpers/serviceBusHelper';
import debug from 'debug';
const serviceBusDebugger = debug('app:serviceBus');
import { getEvent } from '../../helpers/eventHelper';
import { commonEvent } from '../../constants/eventConstant';

export const model = 'esdCustomers';

export const servedEvent = [
  userEvent.UPDATED,
  esdCustomerEvent.UPDATED,
  esdCustomerEvent.CREATED,
  esdPurchaseOrderEvent.CREATED,
];

export const handle = async (event, eventHandleCallBack) => {
  try {
    const DataModel = mongoose.model(model);
    const taskList = [];

    switch (event.type) {
      case esdPurchaseOrderEvent.CREATED:  {
        let data = event.payload.newData;
        // console.log('hungnv', data);
        
        const customerInfo = await DataModel.findOne({ 'permissionList.userId' : data.createdBy}, 
        {
          customerOracleCode: 1,
          customerFtgsmId: 1,
          customerName: 1,
          salesteamId: 1,
          salesteamOracleCode: 1,
          salesteamFtgsmId: 1,
          salesteamName: 1,
          salesmanName: 1,
          paymentTerm: 1,
          companyOracleCode: 1,
          companyFtgsmId: 1,
          companyName: 1,
          departmentOracleCode: 1,
          departmentFtgsmId: 1,
          departmentName: 1,
          subInventoryOracleCode: 1,
          subInventoryFtgsmId: 1,
          subInventoryName: 1,
          billToOracleCode: 1,
          billToName: 1,
          shipToOracleCode: 1,
          shipToName: 1,
          priceLevel: 1,
        });
        console.log('customerInfo', customerInfo)
        let combine = {};
        Object.entries(customerInfo).forEach(([key, value]) => {
          if (key !== '_id' && key == '_doc') {
            // console.log('key-value', key);
            Object.entries(value).forEach(([key1, value1]) => {
              combine[key1] = value1;
            })
          }
        });
        Object.entries(data).forEach(([key, value]) => {
          if (key !== '_id') {
            // console.log('key-value', key, value);
            if (key == 'salesOrderLineAll') {
              combine['orderLineAll'] = value;
            } else {
              combine[key] = value;
            }
          }
        });
        
        combine['customerId'] = customerInfo._id;

        delete combine["_id"];
        // console.log('combine', combine);
        const eventType = getEvent(model, commonEvent.COMBINE_PURCHASE);
        
        sb.publish(model, eventType, { model, oldData: {}, newData: combine }, '0', (err) => {
          if (err) {
            serviceBusDebugger(`[SB] Publish error: ${colors.red(err)}`);
          }
        });
        
        
        break;
      }
      
      // case esdCustomerEvent.CREATED: 
      // case esdCustomerEvent.UPDATED: 
      // {
      //   const esdCustomer = event.payload.newData;
      //   // console.log("esdCustomer_check", esdCustomer.userIdList);

      //   const userList = [];
      //   esdCustomer.userIdList.forEach(userId => {
      //     userList.push({
      //       'userId': userId,
      //       'canRead': true,
      //     })
      //   });

      //   esdCustomer.userIdList.forEach(userId => {
      //     taskList.push((cb) => {
      //       DataModel.update(
      //         { _id: esdCustomer._id },
      //         {
      //           'permissionList': userList,
      //         },
      //         { multi: true },
      //         cb,
      //       );
      //     });
      //   });
        

      //   break;
      // }

      case userEvent.UPDATED: {
        const user = event.payload.newData;

        taskList.push((cb) => {
          DataModel.update(
            { createdBy: user._id },
            {
              createdByUserName: user.userName,
              createdByFullName: user.fullName,
            },
            { multi: true },
            cb,
          );
        });

        taskList.push((cb) => {
          DataModel.update(
            { updatedBy: user._id },
            {
              updatedByUserName: user.userName,
              updatedByFullName: user.fullName,
            },
            { multi: true },
            cb,
          );
        });

        break;
      }

      default: {
        break;
      }
    } // switch (event.type)

    if (taskList.length > 0) {
      async.series(taskList, eventHandleCallBack);
    } else {
      eventHandleCallBack(null);
    }
  } catch (error) {
    eventHandleCallBack(error);
  }
};
