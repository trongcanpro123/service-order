import mongoose from 'mongoose';
import async from 'async';
import userEvent from '../events/userEvent';
import esdCustomerEvent from '../events/esdCustomerEvent';
import sb from '../../helpers/serviceBusHelper';
import debug from 'debug';
const serviceBusDebugger = debug('app:serviceBus');
import { getEvent } from '../../helpers/eventHelper';
import { commonEvent } from '../../constants/eventConstant';
import { getAutoIncreaseCode } from '../../helpers/commonHelper';
import { ORDER_NUMBER_PATTERN_LINEID } from '../constants/orderConstant';

export const model = 'esdProducts';

export const servedEvent = [
  userEvent.UPDATED,
  esdCustomerEvent.COMBINE_PURCHASE,
];

export const handle = (event, eventHandleCallBack) => {
  try {
    const DataModel = mongoose.model(model);
    const taskList = [];

    switch (event.type) {
      case esdCustomerEvent.COMBINE_PURCHASE:  {
        const data = event.payload.newData;
        let orderInfo = {};
        Object.entries(data).forEach(async([key, value]) => {
          if (key == 'orderLineAll') {
            const orderLineAll = [];
            let total = 0;
            
            // value.forEach( async (rs) => {
            for (let i = 0; i < value.length; i += 1) {
              let item = {};
              const rs = value[i];
              console.log(rs);
              var productInfo = await DataModel.findOne({partNumber: rs.partNumber});
              item['partNumber'] = rs.partNumber;
              item['state'] = 'pendingLicence';
              item['quantity'] = rs.quantity;
              item['productId'] = productInfo._id;
              item['oracleCode'] = productInfo.oracleCode;
              item['ftgsmId'] = productInfo.ftgsmId;
              item['ftgsmTaxId'] = productInfo.ftgsmTaxId;
              item['productName'] = productInfo.productName;
              let price = data.priceLevel == 'unitPriceL3' ? productInfo.unitPriceL3 : (data.priceLevel == 'unitPriceL2' ? productInfo.unitPriceL2 : productInfo.unitPrice);
              item['unitPrice'] = price;
              item['lineId'] = await getAutoIncreaseCode(ORDER_NUMBER_PATTERN_LINEID)
              total += price * rs.quantity;
              orderLineAll.push(item);
            }
            orderInfo[key] = orderLineAll;
          } else {
            orderInfo[key] = value;
          }
          if (Object.keys(data).length == Object.keys(orderInfo).length){
            const eventType = getEvent(model, commonEvent.COMBINE_PURCHASE);
        
            sb.publish(model, eventType, { model, oldData: {}, newData: orderInfo }, '0', (err) => {
              if (err) {
                serviceBusDebugger(`[SB] Publish error: ${colors.red(err)}`);
              }
            });
          }
        })

        break;
      }
      case userEvent.UPDATED: {
        const user = event.payload.newData;

        taskList.push((cb) => {
          DataModel.update(
            { createdBy: user._id },
            {
              createdByUserName: user.userName,
              createdByFullName: user.fullName,
            },
            { multi: true },
            cb,
          );
        });

        taskList.push((cb) => {
          DataModel.update(
            { updatedBy: user._id },
            {
              updatedByUserName: user.userName,
              updatedByFullName: user.fullName,
            },
            { multi: true },
            cb,
          );
        });

        break;
      }

      default: {
        break;
      }
    } // switch (event.type)

    if (taskList.length > 0) {
      async.series(taskList, eventHandleCallBack);
    } else {
      eventHandleCallBack(null);
    }
  } catch (error) {
    eventHandleCallBack(error);
  }
};
