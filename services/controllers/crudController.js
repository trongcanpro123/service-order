import {
  idParamController,
  swaggerController,
  queryController, readController,
  createController, updateController, deleteController,
} from '../../helpers/controllerHelper';

import healthcheck from '../../helpers/healthHelper';

const router = require('express').Router();

router.get('/healthcheck', healthcheck);

router.get('/swagger', swaggerController);

router.param('id', idParamController);

router.get('/', queryController);

router.get('/:id', readController);

router.post('/', createController);

router.put('/:id', updateController);

router.delete('/:id', deleteController);

module.exports = router;
