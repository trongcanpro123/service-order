const router = require('express').Router();

const autoCrudModel = [
  '/v2/esdSalesteams',
  '/v2/esdPurchaseOrders',
  '/v2/user',
  '/v2/sOrders'
];

router.use('/v2/uuids', require('./uuidController'));
router.use(autoCrudModel, require('./crudController'));

module.exports = router;
