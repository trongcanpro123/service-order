const mongoose = require('mongoose');
import { ORDER_STATE_LIST, ORDER_NUMBER_PATTERN, ORDER_NUMBER_LENGTH, ORDER_STATE_SM_LIST } from '../constants/orderConstant';
import { getSequenceCode } from '../../helpers/commonHelper';

const s_orderSchema = new mongoose.Schema({
  orderNumber: { type: String, unique: true },
  active: { type: Boolean, required: true, default: true },

  createdAt: { type: Date },
  createdBy: { type: mongoose.Schema.Types.ObjectId },
  createdByUserName: String,
  createdByFullName: String,
  createdByEmail: String,
  createdByPhone: String,

  updatedAt: Date,
  updatedBy: { type: mongoose.Schema.Types.ObjectId },
  updatedByUserName: String,
  updatedByFullName: String,

  deleteInProcess: { type: Boolean, default: false },
  deleteRejectTimeOut: { type: Date },
  deleted: { type: Boolean, default: false },
  deletedAt: Date,
  deletedBy: { type: mongoose.Schema.Types.ObjectId },
  deletedByUserName: String,
  deletedByFullName: String,
}, { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } });


s_orderSchema.pre('save', async function (next) {
  try {
    if (!this.orderNumber) {
      this.orderNumber = await getSequenceCode(ORDER_NUMBER_PATTERN, ORDER_NUMBER_LENGTH);
    }

    next();
  } catch (error) {
    console.log('error', error);
    next(error);
  }
});

mongoose.model('sOrders', s_orderSchema, 'sOrders');
