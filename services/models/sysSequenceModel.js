import mongoose from 'mongoose';

const sysSequenceSchema = new mongoose.Schema({
  model: {
    type: String,
    unique: true,
    required: true,
    index: true,
  },

  nextValue: {
    type: Number,
    required: true,
    default: 1,
  },
}, { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } });

mongoose.model('sysSequences', sysSequenceSchema, 'sysSequences');
