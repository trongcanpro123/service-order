const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  userName: { type: String, unique: true, required: true },
  fullName: { type: String, required: true },

  hash: String,
  salt: String,

  email: { type: String, required: true },
  phone: String,
  biography: String,
  avatarFileId: { type: mongoose.Schema.Types.ObjectId },

  roleList: [{
    roleId: { type: mongoose.Schema.Types.ObjectId },
    roleCode: { type: String },
    roleName: { type: String },

    functionList: [{
      functionId: { type: mongoose.Schema.Types.ObjectId },
      functionUrl: { type: String },
      functionName: { type: String },
      functionOrder: { type: Number },

      functionParentId: { type: mongoose.Schema.Types.ObjectId },
      functionParentUrl: { type: String },
      functionParentName: { type: String },

      moduleId: { type: mongoose.Schema.Types.ObjectId },
      moduleCode: { type: String },
      moduleName: { type: String },
      moduleOrder: { type: Number },
      applicationCode: { type: String },

      functionActionList: { type: String },
    }],
  }],

  functionList: [{
    functionId: { type: mongoose.Schema.Types.ObjectId },
    functionUrl: { type: String },
    functionName: { type: String },
    functionOrder: { type: Number },

    functionParentId: { type: mongoose.Schema.Types.ObjectId },
    functionParentUrl: { type: String },
    functionParentName: { type: String },

    moduleId: { type: mongoose.Schema.Types.ObjectId },
    moduleCode: { type: String },
    moduleName: { type: String },
    moduleOrder: { type: Number },
    applicationCode: { type: String },

    functionActionList: { type: String },
  }],

  moduleList: [{
    moduleId: { type: mongoose.Schema.Types.ObjectId },
    moduleCode: { type: String },
    moduleName: { type: String },
    moduleOrder: { type: Number },
    applicationCode: { type: String },
  }],

  departmentList: [{
    companyId: { type: mongoose.Schema.Types.ObjectId, required: true, index: true },
    companyCode: { type: String },
    companyName: { type: String },

    departmentId: { type: mongoose.Schema.Types.ObjectId, required: true, index: true },
    departmentCode: { type: String },
    departmentName: { type: String },
  
    departmentTypeId: { type: mongoose.Schema.Types.ObjectId, required: true, index: true },
    departmentTypeCode: { type: String },
    departmentTypeName: { type: String },

    departmentManagerUserId: { type: mongoose.Schema.Types.ObjectId },
    departmentManagerUserName: { type: String },
    departmentManagerFullName: { type: String },
    departmentManagerEmail: { type: String },
    departmentManagerPhone: { type: String },

    titleId: { type: mongoose.Schema.Types.ObjectId, required: true },
    titleCode: { type: String },
    titleName: { type: String },

    positionId: { type: mongoose.Schema.Types.ObjectId, required: true },
    positionCode: { type: String },
    positionName: { type: String },
  
    divisionId: { type: mongoose.Schema.Types.ObjectId },
    divisionCode: { type: String },
    divisionName: { type: String },

    divisionTypeId: { type: mongoose.Schema.Types.ObjectId },
    divisionTypeCode: { type: String },
    divisionTypeName: { type: String },

    divisionManagerUserId: { type: mongoose.Schema.Types.ObjectId },
    divisionManagerUserName: { type: String },
    divisionManagerFullName: { type: String },
    divisionManagerEmail: { type: String },
    divisionManagerPhone: { type: String },
  }],

  skillList: [{
    skillId: { type: mongoose.Schema.Types.ObjectId },
    skillCode: { type: String },
    skillName: { type: String },

    level: { type: String },
  }],

  certificateList: [{
    certificateId: { type: mongoose.Schema.Types.ObjectId, required: true },
    certificateCode: { type: String },
    certificateName: { type: String },

    name: { type: String, required: true },
    issuer: { type: String, required: true },
    level: { type: String },
    issueDate: { type: Date },
    validTo: { type: Date },
  }],
  
  familyMemberList: [{
    relationshipId: { type: mongoose.Schema.Types.ObjectId, required: true },
    relationshipCode: { type: String },
    relationshipName: { type: String },

    memberName: { type: String, required: true },
    dateOfBirth: { type: Date },
    address: { type: String },

    companyName: { type: String },
    positionTitle: { type: String },
  }],
  
  productSegmentList: [{
    productSegmentId: { type: mongoose.Schema.Types.ObjectId },
    productSegmentCode: { type: String },
    productSegmentName: { type: String },
    canRead: { type: Boolean, default: true },
    canUpdate: { type: Boolean, default: false },
    canDelete: { type: Boolean, default: false },
  }],

  vendorList: [{
    vendorId: { type: mongoose.Schema.Types.ObjectId },
    vendorCode: { type: String },
    vendorName: { type: String },
    canRead: { type: Boolean, default: true },
    canUpdate: { type: Boolean, default: true },
    canDelete: { type: Boolean, default: true },
  }],

  customerList: [{
    customerId: { type: mongoose.Schema.Types.ObjectId },
    customerCode: { type: String },
    customerName: { type: String },
  }],

  canLoginByLocalAccount: { type: Boolean, default: true },
  canLoginByFPTAccount: { type: Boolean, default: true },
  isAdmin: { type: Boolean, default: false },

  active: { type: Boolean, default: true },

  createdAt: { type: Date },
  createdBy: { type: mongoose.Schema.Types.ObjectId },
  createdByUserName: String,
  createdByFullName: String,

  updatedAt: { type: Date },
  updatedBy: { type: mongoose.Schema.Types.ObjectId },
  updatedByUserName: String,
  updatedByFullName: String,

  deleteRejectTimeOut: { type: Date },
  deleted: { type: Boolean, default: false },
  deletedBy: { type: mongoose.Schema.Types.ObjectId },
  deletedByUserName: String,
  deletedByFullName: String,
}, { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } });

mongoose.model('users', userSchema, 'users');
