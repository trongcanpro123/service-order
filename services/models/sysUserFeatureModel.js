import mongoose from 'mongoose';
import { USER_FEATURE_LIST } from '../../constants/policyConstant';

const sysUserFeatureSchema = new mongoose.Schema({
  userId: { type: mongoose.Schema.Types.ObjectId, index: true },
  userName: String,
  fullName: String,
  email: String,

  userFeatureList: [{
    featureName: { type: String }, // enum: USER_FEATURE_LIST
    value: { type: Array },
  }],

  createdAt: { type: Date },
  updatedAt: { type: Date },
}, { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } });

mongoose.model('sysUserFeatures', sysUserFeatureSchema, 'sysUserFeatures');
