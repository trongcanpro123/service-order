import mongoose from 'mongoose';

const serviceBusEventSchema = new mongoose.Schema({
  origin: { type: String, required: true },
  type: { type: String, required: true },
  correlationId: { type: String, required: true, index: true },
  payload: { type: Object, required: true },

  comsumerList: [{
    name: { type: String, required: true },
    handled: { type: Boolean },
    description: { type: String },
    updatedAt: Date,
  }],

  createdAt: { type: Date },
  // createdBy: { type: mongoose.Schema.Types.ObjectId },
  // createdByUserName: String,
  // createdByFullName: String,

  updatedAt: Date,
  // updatedBy: { type: mongoose.Schema.Types.ObjectId },
  // updatedByUserName: String,
  // updatedByFullName: String,

  deleteRejectTimeOut: { type: Date },
  deleted: { type: Boolean, default: false },
  deletedAt: Date,
  deletedBy: { type: mongoose.Schema.Types.ObjectId },
  deletedByUserName: String,
  deletedByFullName: String,
}, { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } });

mongoose.model('serviceBusEvents', serviceBusEventSchema, 'serviceBusEvents');
