import mongoose from 'mongoose';
// import { USER_FEATURE_LIST } from '../../constants/policyConstant';
// import { OPERATOR_LIST } from '../../constants/dataTypeConstant';

const schema = new mongoose.Schema({
  userId: { type: mongoose.Schema.Types.ObjectId, required: true, index: true},
  userName: { type: String, required: true, index: true },
  fullName: { type: String, required: true, index: true },

  policyId: { type: mongoose.Schema.Types.ObjectId, required: true, index: true},
  policyName: { type: String, required: true, index: true },

  functionId: { type: mongoose.Schema.Types.ObjectId, required: true, index: true},
  functionUrl: { type: String, required: true, index: true },
  functionName: { type: String, required: true, index: true },

  serviceId: { type: mongoose.Schema.Types.ObjectId },
  serviceCode: { type: String, required: true, index: true },
  serviceName: { type: String, required: true, index: true },

  actionId: { type: mongoose.Schema.Types.ObjectId },
  actionCode: { type: String, required: true, index: true },
  path: { type: String, required: true, index: true },
  method: { type: String, required: true, index: true },

  userFeatureList: [{
    featureName: { type: String }, // enum: USER_FEATURE_LIST
    value: { type: Array },
  }],

  allowedRequestFieldList: Array,
  allowedResponseFieldList: Array,

  recordFeatureList: [{
    featureName: String,
    type: { type: String },
    selectedOperator: { type: String }, // enum: OPERATOR_LIST

    isUserFeature: Boolean,
    selectedValueList: { type: String }, // String-like ARRAY, separated by ","
  }],

  createdAt: { type: Date },
  updatedAt: Date,
}, { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } });

mongoose.model('sysAccessList', schema, 'sysAccessList');
