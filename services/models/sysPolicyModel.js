import mongoose from 'mongoose';

import { USER_FEATURE_LIST } from '../../constants/policyConstant';
import { OPERATOR_LIST } from '../../constants/dataTypeConstant';

export const policySchema = new mongoose.Schema({
  policyName: { type: String, required: true },

  functionId: { type: mongoose.Schema.Types.ObjectId, required: true, index: true },
  functionUrl: { type: String, required: true, index: true },
  functionName: { type: String, required: true, index: true },

  serviceId: { type: mongoose.Schema.Types.ObjectId, required: true, index: true },
  serviceCode: { type: String, required: true, index: true },
  serviceName: { type: String, required: true, index: true },

  actionList: { type: Array, defaultValue: [] }, // service's actionList
  fieldList: { type: Array, defaultValue: [] }, // service's fieldList

  actionId: { type: mongoose.Schema.Types.ObjectId, required: true, index: true },
  actionCode: { type: String, required: true, index: true },

  path: { type: String, required: true, index: true },
  method: { type: String, required: true, index: true },

  userFeatureList: [{
    featureName: { type: String, enum: USER_FEATURE_LIST },

    operatorOptionList: { type: Array },
    selectedOperator: { type: String, enum: OPERATOR_LIST },

    valueOptionList: { type: Array },
    selectedValueList: { type: Array },
  }],

  fullRequestFieldList: { type: Array },
  requestFieldList: { type: Array },
  requestExceptFieldList: { type: Array },
  allowedRequestFieldList: { type: Array },

  fullResponseFieldList: { type: Array },
  responseFieldList: { type: Array },
  responseExceptFieldList: { type: Array },
  allowedResponseFieldList: { type: Array },

  recordFeatureList: [{
    featureName: { type: String },
    type:{ type: String },

    operatorOptionList: { type: Array },
    selectedOperator: { type: String, enum: OPERATOR_LIST },

    isUserFeature: Boolean,
    selectedValueList: { type: String }, // String-like ARRAY, separated by ","
  }],

  active: { type: Boolean },

  createdAt: { type: Date },
  createdBy: { type: mongoose.Schema.Types.ObjectId },
  createdByUserName: String,
  createdByFullName: String,

  updatedAt: Date,
  updatedBy: { type: mongoose.Schema.Types.ObjectId },
  updatedByUserName: String,
  updatedByFullName: String,

}, { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } });

mongoose.model('sysPolicies', policySchema, 'sysPolicies'); // CLONE policy config for each service
