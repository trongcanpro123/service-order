FROM node:10.19.0-alpine3.11
MAINTAINER N.T.CAN<cannt3@fpt.com.vn>

RUN apk add --no-cache bash
RUN mkdir /service
WORKDIR /service

# COPY package.json /service/
# COPY package-lock.json /service/
# COPY wait-for-it.sh /service/
# COPY ./ /service/

# ENV DEBUG=app:db
# ENV SERVICE_PORT=3000
# ENV DATABASE=mongodb://10.7.0.119:27017/crm
# ENV MESSAGE_BROKER_SERVER=amqp://10.7.0.108?heartbeat=60
# ENV EXCHANGE_NAME=synnexfpt

ENV V_API=v2_

# RUN npm install

RUN npm install forever -g

# RUN sed -i 's/nextObject/next/g' /service/node_modules/gridfs-stream/lib/index.js

EXPOSE 3000
CMD ["npm", "start"]
HEALTHCHECK --interval=120s CMD wget -qO- localhost:3000/v2/payment/healthcheck
