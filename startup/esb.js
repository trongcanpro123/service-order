import config from 'config';
import debug from 'debug';
import _ from 'lodash';
import async from 'async';
import mongoose from 'mongoose';

import sb from '../helpers/serviceBusHelper';
import { handleCommonEvent } from '../helpers/eventHelper';
import eventHandlerList from '../services/eventHandlers';
import commonEvent from '../constants/eventConstant';
import allEventList from '../services/events';

const serviceBusDebugger = debug('app:sb');
const serviceBusEventModelName = 'serviceBusEvents';

const serviceBusEventHandle = (event, eventHandleCallBack) => {
  try {
    // const ServiceBusEventModel = mongoose.model(serviceBusEventModelName);

    // handleCommonEvent(ServiceBusEventModel, serviceBusEventModelName, event, taskList);

    const taskList = [];

    const {
      origin, type,
      payload, correlationId,
    } = event;

    const eventType = type.split('.')[2];

    // if ((eventType === commonEvent.HANDLED) || (eventType === commonEvent.CAN_NOT_HANDLED)) {
    //   taskList.push((cb) => {
    //     ServiceBusEventModel.updateOne(
    //       { correlationId },
    //       {
    //         $push: {
    //           comsumerList: {
    //             name: origin,
    //             handled: eventType === commonEvent.HANDLED,
    //             description: payload.error ? JSON.stringify(payload.error) : '',
    //             updatedAt: new Date(),
    //           },
    //         }
    //       },
    //       cb,
    //     );
    //   });
    // } else {
    //   taskList.push((cb) => {
    //     const serviceBusEvent = new ServiceBusEventModel();

    //     serviceBusEvent.origin = origin;
    //     serviceBusEvent.type = type;
    //     serviceBusEvent.payload = payload;
    //     serviceBusEvent.correlationId = correlationId;

    //     serviceBusEvent.save(cb);
    //   });
    // }

    if (taskList.length > 0) {
      async.series(taskList, eventHandleCallBack);
    } else {
      eventHandleCallBack(null);
    }
  } catch (error) {
    eventHandleCallBack(error);
  }
};

sb.init(
  config.get('messageBroker.server'),
  config.get('messageBroker.exchangeName'),
  (err) => {
    if (err) {
      throw new Error('CannotInitServiceBus');
    } else {
      serviceBusDebugger(`[SB] Connected!`);

      eventHandlerList.forEach((eventHandler) => {
        const { model, servedEvent, handle } = eventHandler;

        sb.consume(model, servedEvent, handle, (error) => {
          if (error) {
            console.error(`[SB] ${model} handled ${servedEvent} but get error: \n ${error}}`);
          }
        });

        serviceBusDebugger(`[SB] Queue [${model}] is registered.`);
      });

      // sb.consume(serviceBusEventModelName, allEventList, serviceBusEventHandle, (error) => {
      //   if (error) {
      //     console.error(`[SB] serviceBusEvent consumer gets error: \n ${error}}`);
      //   }
      // });
    }
  }
);
