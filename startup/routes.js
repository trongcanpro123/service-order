import helmet from 'helmet';
import morgan from 'morgan';
import cors from 'cors';
import bodyParser from 'body-parser';
import errorhandler from 'errorhandler';

module.exports = function(app) {
  app.use(helmet());
  app.use(helmet.noCache());
  app.use(cors());
  app.use(morgan('dev'));

  app.use(bodyParser.json({limit: '50mb'}) );
  app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: false,
    parameterLimit:50000
  }));

  app.use(require('method-override')());

  app.use(require('../helpers/authHelper'));
  app.use(require('../services/controllers'));

  app.use((req, res, next) => {
    const err = new Error(`Not found: ${req.originalUrl}`);
    err.status = 404;
    next(err);
  });

  app.use(errorhandler());
}
